function [xyz] = Readxyz(filename)
% This function reads binary files containing the coordinates from the 
% extract-plane output function of EllipSys (ie .p*.xyz)

% Open 
fid=fopen(filename);

% Read the number of points along each direction 
ndum=fread(fid,1,'int32'); nj=fread(fid,1,'int32'); nk=fread(fid,1,'int32');

% Read xyz data from file
xyz = zeros(nj,nk);

for i = 1:nj
    % Move cursor by 8 bytes

    status=fseek(fid,8,0);
    xyz(i,:) = fread(fid,3,'float64');
end


st=fclose(fid);
