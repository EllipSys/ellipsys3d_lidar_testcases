function [out] = Readuvw(filename)
% This function reads binary files containing the variables outputted from 
% the extract-plane output function of EllipSys (ie .*.p*.f)

% Open 
fid=fopen(filename);

% Read the number of points along each direction 
ndum=fread(fid,1,'int32'); nj=fread(fid,1,'int32'); nk=fread(fid,1,'int32');

% Read lidar output data from file
out = zeros(nj,nk);

for i = 1:nj
    % Move cursor by 8 bytes
    status=fseek(fid,8,0);
    % FORMAT: vlos(i),dvlos(i),wtot(i),uvwlos(i,1),uvwlos(i,2),uvwlos(i,3)
    out(i,:) = fread(fid,6,'float64');
end


st=fclose(fid);